module.exports = {
  content: ['./src/**/*.{js,html,vue}', 'public/**/*.html'],
  theme: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
